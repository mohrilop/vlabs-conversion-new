# Conversion of IITB experiments to UI3

This projects is based on a similar project from IITB for populating experiment data to UI.  Here we will replace the IITB UI with the UI3.0.

The IITB project comes from [here](http://vlabs.iitb.ac.in/gitlab/jai/vlabs-conversion-script.git)